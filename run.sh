#!/bin/bash
MYSQL_DATA=/home/ces/docker/mysql/testingv6
MYSQL_DATA_DOCKER=/var/lib/mysql
POSTGRESQL_DATA=/home/ces/docker/postgresql/testingv6
POSTGRESQL_DATA_DOCKER=/var/lib/postgresql/data

# PostgreSQL env
POSTGRES_PASSWORD=postgres
POSTGRES_USER=postgres
POSTGRES_DB=geo

# MySQL env
MYSQL_ROOT_PASSWORD=root
MYSQL_DATABASE=lacnic


sudo docker run -p 3306:3306 -v $MYSQL_DATA:$MYSQL_DATA_DOCKER -e "MYSQL_DATABASE=$MYSQL_DATABASE" -e "MYSQL_ROOT_PASSWORD=$MYSQL_ROOT_PASSWORD" -d mysql-lacnic 
sudo docker run -p 5432:5432 -v $POSTGRESQL_DATA:$POSTGRESQL_DATA_DOCKER -e "POSTGRES_USER=$POSTGRES_PASSWORD" -e"POSTGRES_PASSWORD=$POSTGRES_PASSWORD" -e"POSTGRES_DB=$POSTGRES_DB" -d postgres:9.5
sudo docker run -p 8080:8080 -d jboss-lacnic


# sudo docker pull postgres:9.5
# sudo docker pull mysql
# sudo docker pull 
#


# build
cd mysql && sudo docker build -t mysql-lacnic .
cd .. &&  cd jboss && sudo docker build -t jboss-lacnic .




